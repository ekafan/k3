import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public class DoubleStack {

    private final LinkedList<Double> list;
    private static final LinkedList<String> correctOp =
            new LinkedList<>(Arrays.asList("+", "-", "/", "*", "SWAP", "ROT", "DUP"));

    public LinkedList<Double> getList() {
        return list;
    }

    public static void main(String[] argum) {
        DoubleStack myList = new DoubleStack();

        System.out.println("myList before adding elements : " + myList);
        myList.push(12.1);
        myList.push(15.2);
        myList.push(45.3);
        myList.push(2.6);
        System.out.println("myList after adding elements : " + myList);

        System.out.println("First element in myList : " + myList.tos());

        Object cloneList = null;
        try {
            cloneList = myList.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        System.out.println("cloneList : " + cloneList);

        System.out.println("myList equals cloneList : " + myList.equals(cloneList));

        System.out.println("myList is empty : " + myList.stEmpty());

        System.out.println(myList.tos());

        System.out.println(myList);

        System.out.println(myList.pop());
        System.out.println(myList.pop());

        System.out.println("myList : " + myList);
        System.out.println("cloneList : " + cloneList);

        DoubleStack myList2 = new DoubleStack();
        System.out.println(myList2);
        System.out.println(myList);

        System.out.println(myList.equals(myList2));

        myList.push(23.1);
        myList.push(10.2);
        myList.push(5.6);
        myList.push(2.4);
        System.out.println(myList);

        myList.op("+");
        System.out.println(myList);
        myList.op("-");
        System.out.println(myList);
        myList.op("*");
        System.out.println(myList);
        myList.op("/");
        System.out.println(myList);

        myList2.push(5.0);
        myList2.push(3.0);
        myList2.op("-");
        System.out.println(myList2);

        String s = "35. 10. -3. + /";
        System.out.println(interpret(s));
    }

    public DoubleStack() {
        list = new LinkedList<>();
    }

    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Override
    public Object clone() throws CloneNotSupportedException {
        DoubleStack cloned = new DoubleStack();

        if (list.size() > 0) {
            for (int i = list.size() - 1; i >= 0; i--) {
                cloned.list.push(list.get(i));
            }
        }
        return cloned;
    }

    public boolean stEmpty() {
        return list.isEmpty();
    }

    public void push(double a) {
        list.push(a);
    }

    public double pop() {
        try {
            return list.pop();
        } catch (NoSuchElementException e) {
            throw new NoSuchElementException("list is empty. pop() cannot be used");
        }
    }

    public void op(String s) {

        if (list.size() > 0) {
            if (s.equals("DUP")) {
                double num = list.pop();
                push(num);
                push(num);
                return;
            }
        }


        if (list.size() < 2) {
            throw new NoSuchElementException("not enough elements in list for : " + s);
        }
        if (!correctOp.contains(s)) {
            throw new IllegalArgumentException("Unknown operation : " + s);
        }

        double num1 = list.pop();
        double num2 = list.pop();

        switch (s) {
            case "+":
                push(num2 + num1);
                break;
            case "-":
                push(num2 - num1);
                break;
            case "*":
                push(num2 * num1);
                break;
            case "/":
                push(num2 / num1);
                break;
            case "SWAP":
                push(num1);
                push(num2);
                break;
            case "ROT":
                if (list.size() < 1) {
                    throw new NoSuchElementException("not enough elements in list for : " + s);
                }
                double num3 = list.pop();
                push(num2);
                push(num1);
                push(num3);
        }
    }

    public double tos() {
        try {
            return list.getFirst();
        } catch (NoSuchElementException e) {
            throw new NoSuchElementException("list is empty. tos() cannot be used");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof DoubleStack) {
            return list.equals(((DoubleStack) o).getList());
        } else
            return false;
    }

    @Override
    public String toString() {

        Collections.reverse(list);
        String print = list.toString();
        Collections.reverse(list);

        return print;
    }

    public static double interpret(String pol) {

        if (pol.length() < 1) {
            throw new RuntimeException("Expression is empty : " + pol);
        }

        boolean isNum = false;
        int opCount = 0;

        String[] elements;
        elements = pol.trim().split("\\s+");

        DoubleStack myStack = new DoubleStack();

        for (String element : elements) {
            try {
                double number = Double.parseDouble(element);
                myStack.push(number);
                isNum = true;
            } catch (NumberFormatException e) {

                if (!correctOp.contains(element)) {
                    throw new RuntimeException("Expression " + pol + " contains illegal symbols : " + element);
                } else if (myStack.getList().size() < 1) {
                    throw new RuntimeException("Expression " + pol + " " +
                            "has not enough elements to use current operation : " + element);
                }

                isNum = false;
                opCount++;
                myStack.op(element);
            }
        }
        if (isNum && opCount > 0) {
            throw new RuntimeException("Expression " + pol + " has redundant elements after the last operation");
        }

        return myStack.tos();
    }
}
